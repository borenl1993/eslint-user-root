package convert

import (
	"crypto/sha256"
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// ESLint reports are grouped by file
type ESLintFileReport struct {
	FilePath string                `json:"filePath"`
	Messages []ESLintVulnerability `json:"messages"`
}

// Messages (Issues) concerning the parent file
type ESLintVulnerability struct {
	Line    int    `json:"line"`
	EndLine int    `json:"endLine"`
	Message string `json:"message"`
	RuleId  string `json:"ruleId"`
	Source  string `json:"source"`
}

// compareKey returns a string used to establish whether two issues are the same.
func (m *ESLintVulnerability) compareKey(r ESLintFileReport) string {
	return strings.Join([]string{r.FilePath, m.Fingerprint(), m.RuleId}, ":")
}

// Fingerprint calculates the checksum of the affected code.
func (m *ESLintVulnerability) Fingerprint() string {
	// Cleanup the source code extract from the report
	code := strings.TrimSpace(m.Source)

	// create code fingerprint using SHA256
	h := sha256.New()
	h.Write([]byte(code))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// location returns a structured location
func (m *ESLintVulnerability) location(prependPath string, r ESLintFileReport) issue.Location {
	return issue.Location{
		File:      filepath.Join(prependPath, r.FilePath),
		LineStart: m.Line,
		LineEnd:   m.EndLine,
	}
}

// identifiers returns the normalized identifiers of the vulnerability.
func (m *ESLintVulnerability) identifiers() []issue.Identifier {
	return []issue.Identifier{
		m.eSLintIdentifier(),
	}
}

// fSBIdentifier returns a structured Identifier for a FSB bug Type
func (m *ESLintVulnerability) eSLintIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "eslint_rule_id",
		Name:  fmt.Sprintf("ESLint rule ID %s", m.RuleId),
		Value: m.RuleId,
		URL:   m.url(),
	}
}

func (m *ESLintVulnerability) url() string {
	switch {
	case strings.HasPrefix(m.RuleId, "security/"):
		ruleIDShort := strings.TrimPrefix(m.RuleId, "security/")
		return fmt.Sprintf("https://github.com/nodesecurity/eslint-plugin-security#%s", ruleIDShort)
	case strings.HasPrefix(m.RuleId, "react/"):
		ruleIDShort := strings.TrimPrefix(m.RuleId, "react/")
		return fmt.Sprintf("https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/%s.md", ruleIDShort)
	default:
		return ""
	}
}
